<?php

require_once 'Auditing.php';

class Archive_type
{
    public $id;
    public $name;
    public $description;
    public $dbh;

    function __construct($dbh)
    {
    $this->dbh = $dbh;
    }

    // Create Archive_type
    function registerarchive_type() 
    {
        $cons = "INSERT INTO archive_type VALUES(?,?,?)";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id);
        $prep->bindparam(2, $this->name);
        $prep->bindparam(3, $this->description);
        

        try {
            $prep->execute();
            $this->id = $this->dbh->lastInsertId();
            return $this->id;
        } 
        catch (Exception $e) {
            return false;
        }
    }

//	// Read all archive_type
        function readArchive_type() 
        {
            $i = 0;
            $arrayData = [];
            $cons = "SELECT * FROM archive_type";
            $prep = $this->dbh->prepare($cons);
            try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //$i++;
                $arrayData[$i]['id'] = $reg->id;
                $arrayData[$i]['nome'] = $reg->name;
                $arrayData[$i]['morada'] = $reg->description;
                $i++;
            }
                return $arrayData;
            } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
                        return false;
            }
        
          }

	
   //Updating archive_type
    function updateArchive_type()
    {
        $cons = "UPDATE archive_type SET name = ?,description = ? WHERE id=?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->name);
        $prep->bindparam(2, $this->description);
        $prep->bindparam(3, $this->id);

        try{
            $prep->execute();

            return true;
        } 
        catch (Exception $e) {

            return $e->getMessage();
        }
    }

//	
//	// Delete archive_type
    function deleteArchive_type()
    {
        $cons = "DELETE FROM archive_type WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);

        try {
            $prep->execute();
            //record deleted
            return true;
        } 
        catch (Exception $e) {

            return false;
        }
    }

//	
	
}

?>
