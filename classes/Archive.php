<?php

require_once 'Auditing.php';

class Archive
{
    public $id;
    public $title;
    public $description;
    public $date;
    public $id_archive_type;
    public $dbh;

    function __construct($dbh)
    {
    $this->dbh = $dbh;
    }

    // Create Archive
    function registerArchive() 
    {
        $cons = "INSERT INTO archive VALUES(?,?,?,?,?)";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id);
        $prep->bindparam(2, $this->title);
        $prep->bindparam(3, $this->description);
        $prep->bindparam(4, $this->date);
        $prep->bindparam(5, $this->id_archive_type);
        

        try {
            $prep->execute();
            $this->id = $this->dbh->lastInsertId();
            return $this->id;
        } 
        catch (Exception $e) {
            return false;
        }
    }

//	// Read all archive
        function readArchive() 
        {
            $i = 0;
            $arrayData = [];
            $cons = "SELECT * FROM archive";
            $prep = $this->dbh->prepare($cons);
            try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //$i++;
                $arrayData[$i]['id'] = $reg->id;
                $arrayData[$i]['title'] = $reg->title;
                $arrayData[$i]['description'] = $reg->description;
                $arrayData[$i]['date'] = $reg->date;
                $arrayData[$i]['id_archive_type'] = $reg->id_archive_type;
                $i++;
            }
                return $arrayData;
            } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
                        return false;
            }
        
          }

	
   //Updating archive
    function updateArchive()
    {
        $cons = "UPDATE archive SET title = ?,description = ?,date = ?,id_archive_type =  ? WHERE id = ?";
        $prep= $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->title);
        $prep->bindparam(2, $this->description);
        $prep->bindparam(3, $this->date);
        $prep->bindparam(4, $this->id_archive_type);
        $prep->bindparam(5, $this->id);

        try{
            $prep->execute();

            return true;
        } 
        catch (Exception $e) {

            return $e->getMessage();
        }
    }

//	
//	// Delete archive
    function deleteArchive()
    {
        $cons = "DELETE FROM archive WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);

        try {
            $prep->execute();
            //record deleted
            return true;
        } 
        catch (Exception $e) {

            return false;
        }
    }

//	
	
}

?>
