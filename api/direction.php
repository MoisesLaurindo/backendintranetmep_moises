<?php //

header("Access-Control-Allow-Origin: *"); 
header("Content-Type: application/json; charset=UTF-8"); 
header("Access-Control-Allow-Methods: POST,GET,PUT,DELETE"); 
header("Access-Control-Max-Age: 3600"); 
header("Access-Control-Allow-Headers: Content-Type, Authorization");
//header("Access-Control-Allow-Headers: Content-Type");


//Moises
require_once "../classes/DatabaseConnection.php";
require_once "../classes/Direction.php";
require_once "../classes/Returned.php";
//require_once "../classes/UserToken.php";
/*spl_autoload_register();*/

//Instance the class DatabaseConnection
$databaseConnection = new DatabaseConnection();
$db = $databaseConnection->tryConnect();
// instance the class Aluno
$direction = new Direction($db);
// instance the class that return results
$returned = new Returned();
// instance the class that create the user's token results
// $userToken = new UserToken();
// Takes raw data from the request 
$json = file_get_contents('php://input');
$data = json_decode($json);
// Get the authorization to access resource
// $token = $userToken->getAuthorization();
//if($token){
	// retrieve de method used
	$method = $_SERVER['REQUEST_METHOD'];
	if('POST' === $method) {
		
			$direction->id = NULL;
			$direction->name = $data->name;
			$direction->description = $data->description;
			
			
	$response = $direction->registerDirection();
				
	if($response) $responseReturned = $returned->returnResult(true,'Direcção registada  com successo',$response);
	else $responseReturned = $returned->returnResult(false,'Direcção não registado',array());
				
                               }
        
        elseif('GET' === $method){
		
			$response = $direction->readDirection(); // Read all directions
			if($response) $responseReturned = $returned->returnResult(true,'Direcção encontrada',$response);
			else $responseReturned = $returned->returnResult(false,'Nenhuma direcção encontrada',array());
		//}else $responseReturned = $returned->returnResult(false,'Acesso ao serviço não autorizado',array());
                                 }
        elseif('PUT' === $method){
		
			// Update directions
			$direction->id = $data->id;
			$direction->name = $data->name;
			$direction->description = $data->description;
			
			
			// Retrieve the response about the update of directions
			$response = $direction->updateDirection();
			// Return the result
			if($response) $responseReturned = $returned->returnResult(true,'Direcção actualizada com successo',array());
			else $responseReturned = $returned->returnResult(false,'Direcção não actualizado',array());
			
                                }
                                
        elseif('DELETE'=== $method){
		
			
			$direction->id = $data->id;
			// Retrieve the response about the delete of directions
			$response = $direction->deleteDirection();
			// Return the result
			if($response) $responseReturned = $returned->returnResult(true,'Direcção eliminada com successo',array());
			else $responseReturned = $returned->returnResult(false,'Direcção não eliminada',array());
		//}else $responseReturned = $returned->returnResult(false,'Acesso ao serviço não autorizado',array());
	                            }
                                    else{
		$responseReturned = $returned->returnResult(false,'Pedido não executado',array());
                                        }
$responseReturned = json_encode($responseReturned);
echo $responseReturned;
http_response_code();
?>