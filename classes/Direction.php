<?php

require_once 'Auditing.php';

class Direction
{
    public $id;
    public $name;
    public $description;
    public $dbh;

    function __construct($dbh)
    {
    $this->dbh = $dbh;
    }

    // Create Direction
    function registerDirection() 
    {
        $cons = "INSERT INTO direction VALUES(?,?,?)";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id);
        $prep->bindparam(2, $this->name);
        $prep->bindparam(3, $this->description);
        

        try {
            $prep->execute();
            $this->id = $this->dbh->lastInsertId();
            return $this->id;
        } 
        catch (Exception $e) {
            return false;
        }
    }

//	// Read all Directions
        function readDirection() 
        {
            $i = 0;
            $arrayData = [];
            $cons = "SELECT * FROM direction";
            $prep = $this->dbh->prepare($cons);
            try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //$i++;
                $arrayData[$i]['id'] = $reg->id;
                $arrayData[$i]['name'] = $reg->name;
                $arrayData[$i]['description'] = $reg->description;
                $i++;
            }
                return $arrayData;
            } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
                        return false;
            }
        
          }

	
   //Updating Direction
    function updateDirection()
    {
        $cons = "UPDATE direction SET name = ?,description = ? WHERE id=?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->name);
        $prep->bindparam(2, $this->description);
        $prep->bindparam(3, $this->id);

        try{
            $prep->execute();

            return true;
        } 
        catch (Exception $e) {

            return $e->getMessage();
        }
    }

//	
//	// Delete Direction
    function deleteDirection()
    {
        $cons = "DELETE FROM direction WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);

        try {
            $prep->execute();
            //record deleted
            return true;
        } 
        catch (Exception $e) {

            return false;
        }
    }

//	
	
}

?>
