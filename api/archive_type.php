<?php //

header("Access-Control-Allow-Origin: *"); 
header("Content-Type: application/json; charset=UTF-8"); 
header("Access-Control-Allow-Methods: POST,GET,PUT,DELETE"); 
header("Access-Control-Max-Age: 3600"); 
header("Access-Control-Allow-Headers: Content-Type, Authorization");
//header("Access-Control-Allow-Headers: Content-Type");


//Moises
require_once "../classes/DatabaseConnection.php";
require_once "../classes/Archive_type.php";
require_once "../classes/Returned.php";
//require_once "../classes/UserToken.php";
/*spl_autoload_register();*/

//Instance the class DatabaseConnection
$databaseConnection = new DatabaseConnection();
$db = $databaseConnection->tryConnect();
// instance the class Aluno
$archive_type = new Archive_type($db);
// instance the class that return results
$returned = new Returned();
// instance the class that create the user's token results
// $userToken = new UserToken();
// Takes raw data from the request 
$json = file_get_contents('php://input');
$data = json_decode($json);
// Get the authorization to access resource
// $token = $userToken->getAuthorization();
//if($token){
	// retrieve de method used
	$method = $_SERVER['REQUEST_METHOD'];
	if('POST' === $method) {
		
			$archive_type->id = NULL;
			$archive_type->name = $data->name;
			$archive_type->description = $data->description;
			
			
	$response = $archive_type->registerarchive_type();
				
	if($response) $responseReturned = $returned->returnResult(true,'Tipo de arquivo registado  com successo',$response);
	else $responseReturned = $returned->returnResult(false,'Tipo de arquivo  não registado',array());
				
                               }
        
        elseif('GET' === $method){
		
			$response = $archive_type->readArchive_type(); // Read all archive_type
			if($response) $responseReturned = $returned->returnResult(true,'Tipo de arquivo encontrado',$response);
			else $responseReturned = $returned->returnResult(false,'Nemhum tipo de arquivo encontrado',array());
		//}else $responseReturned = $returned->returnResult(false,'Acesso ao serviço não autorizado',array());
                                 }
        elseif('PUT' === $method){
		
			// Update candidature
			$archive_type->id = $data->id;
			$archive_type->name = $data->name;
			$archive_type->description = $data->description;
			
			
			// Retrieve the response about the update of candidate
			$response = $archive_type->updateArchive_type();
			// Return the result
			if($response) $responseReturned = $returned->returnResult(true,'Tipo de arquivo actualizado com successo',array());
			else $responseReturned = $returned->returnResult(false,'Tipo de arquivo não actualizado',array());
			
                                }
                                
        elseif('DELETE'=== $method){
		
			
			$archive_type->id = $data->id;
			// Retrieve the response about the delete of candidate
			$response = $archive_type->deleteArchive_type();
			// Return the result
			if($response) $responseReturned = $returned->returnResult(true,'Tipo de arquivo eliminado com successo',array());
			else $responseReturned = $returned->returnResult(false,'Tipo de arquivo  não eliminado',array());
		//}else $responseReturned = $returned->returnResult(false,'Acesso ao serviço não autorizado',array());
	                            }
                                    else{
		$responseReturned = $returned->returnResult(false,'Pedido não executado',array());
                                        }
$responseReturned = json_encode($responseReturned);
echo $responseReturned;
http_response_code();
?>