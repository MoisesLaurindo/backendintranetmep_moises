<?php //

header("Access-Control-Allow-Origin: *"); 
header("Content-Type: application/json; charset=UTF-8"); 
header("Access-Control-Allow-Methods: POST,GET,PUT,DELETE"); 
header("Access-Control-Max-Age: 3600"); 
header("Access-Control-Allow-Headers: Content-Type, Authorization");
//header("Access-Control-Allow-Headers: Content-Type");


//Moises
require_once "../classes/DatabaseConnection.php";
require_once "../classes/Archive.php";
require_once "../classes/Returned.php";
//require_once "../classes/UserToken.php";
/*spl_autoload_register();*/

//Instance the class DatabaseConnection
$databaseConnection = new DatabaseConnection();
$db = $databaseConnection->tryConnect();
// instance the class Aluno
$archive = new Archive($db);
// instance the class that return results
$returned = new Returned();
// instance the class that create the user's token results
// $userToken = new UserToken();
// Takes raw data from the request 
$json = file_get_contents('php://input');
$data = json_decode($json);
// Get the authorization to access resource
// $token = $userToken->getAuthorization();
//if($token){
	// retrieve de method used
	$method = $_SERVER['REQUEST_METHOD'];
	if('POST' === $method) {
		
			$archive->id = NULL;
			$archive->title = $data->title;
			$archive->description = $data->description;
                        $archive->date = $data->date;
			$archive->id_archive_type = $data->id_archive_type;
			
			
	$response = $archive->registerArchive();
				
	if($response) $responseReturned = $returned->returnResult(true,'Arquivo registado  com successo',$response);
	else $responseReturned = $returned->returnResult(false,'Arquivo  não registado',array());
				
                               }
        
        elseif('GET' === $method){
		
			$response = $archive->readArchive(); // Read all archive
			if($response) $responseReturned = $returned->returnResult(true,'Arquivo encontrado',$response);
			else $responseReturned = $returned->returnResult(false,'Nemhum arquivo encontrado',array());
		//}else $responseReturned = $returned->returnResult(false,'Acesso ao serviço não autorizado',array());
                                 }
        elseif('PUT' === $method){
		
			// Update candidature
			$archive->id = $data->id;
			$archive->title = $data->title;
			$archive->description = $data->description;
                        $archive->date = $data->date;
			$archive->id_archive_type = $data->id_archive_type;
			
			
			// Retrieve the response about the update of candidate
			$response = $archive->updateArchive();
			// Return the result
			if($response) $responseReturned = $returned->returnResult(true,'Arquivo actualizado com successo',array());
			else $responseReturned = $returned->returnResult(false,'Arquivo não actualizado',array());
			
                                }
                                
        elseif('DELETE'=== $method){
		
			
			$archive->id = $data->id;
			// Retrieve the response about the delete of candidate
			$response = $archive->deleteArchive();
			// Return the result
			if($response) $responseReturned = $returned->returnResult(true,'Arquivo eliminado com successo',array());
			else $responseReturned = $returned->returnResult(false,'Arquivo não eliminado',array());
		//}else $responseReturned = $returned->returnResult(false,'Acesso ao serviço não autorizado',array());
	                            }
                                    else{
		$responseReturned = $returned->returnResult(false,'Pedido não executado',array());
                                        }
$responseReturned = json_encode($responseReturned);
echo $responseReturned;
http_response_code();
?>